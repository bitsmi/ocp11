package local.variable.type.inference.examples;

// We are able to use var term everywhere because Java is case sensitive
// and this feature allows to use an speciific type like Var and also
// the reserved type name var.
public class Var {
    public void var() {
        var var = "var";
    }
    public void Var() {
        Var var = new Var();
    }
}
