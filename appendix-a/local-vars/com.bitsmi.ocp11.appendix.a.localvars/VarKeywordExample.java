package local.variable.type.inference.examples;

public class VarKeywordExample {

    // If uncommented this code won't compile because it is an instance variable not a local variable
    // var tricky = "Hello";
}
