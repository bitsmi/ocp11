package local.variable.type.inference.examples;

public class DoesThisCompile {

    //  The example won't compile because in
    // (1) Compiler can not know which is type it has to interfere.
    // (2) Compiler can not know also which is the type it has to interfere
    /*public void doesThisCompile(boolean check) {
        var question;   // (1)
        question = 1;
        var answer;     // (2)
        if (check) {
            answer = 2;
        } else {
            answer = 3;
        }
        System.out.println(answer);
    }*/


    /*public void twoTypes {
        int a, var b= 3;    // (1) DOES NOT COMPILE - Multiple variables instances are not allowed.
        var n = null;       // (2) DOES NOT COMPILE - We can not assign directly the null value to var. Compiler won't know which type to assign.
    }*/

    public void  assingNullValues() {
        var n = "myData";   // We declare and assign the var
        n = null;           // but we assign to it a null value

        //  There is an special way to assign null value if we use
        //  a complex type doing the cast.
        var p = (String) null;
    }

    // It won't compile because we are using var with parameter methods and it is not allowed in Java.
    /*public int addition(var a, var b) { // DOES NOT COMPILE
        return a+b;
    }*/


}
