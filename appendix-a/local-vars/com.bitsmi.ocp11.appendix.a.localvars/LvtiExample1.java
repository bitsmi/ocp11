package local.variable.type.inference.examples;

public class LvtiExample1 {

    //  Standard use of with local variables.
    public void whatTypeAmI() {
        var name = "Hello";
        var size = 7;
    }

    //  Ability to do the reassignment
    public void reassignment() {
        var number = 7; // (1)
        number = 4;     // (2)
        // number = "five";  // (3) DOES NOT COMPILE because we are trying to really assign an String to an int primitive type
    }

    //  Lines (1) and (2) will compile because in reality we are assigning compatible types.
    //  Remember that casting with short and byte we can assign a byte value into a short.
    public void conversions() {
        var apples = (short) 10; // (1)
        apples = (byte) 5; // (2)

        // (3) Does not compile because we are trying to assign a int primitive type inside
        //  a short value. When the compiler checks that the type to infer is an int
        //  we will get an error compilation.
        //apples = 1_000_000;
    }

}


