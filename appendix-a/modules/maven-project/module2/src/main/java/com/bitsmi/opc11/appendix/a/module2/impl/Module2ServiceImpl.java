package com.bitsmi.opc11.appendix.a.module2.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Module2ServiceImpl 
{
	Logger log = LoggerFactory.getLogger(Module2ServiceImpl.class);
	
	public String sayHello()
	{
		String message = "Hello from module 2";
		log.info(message);
		
		return message;
	}
}
