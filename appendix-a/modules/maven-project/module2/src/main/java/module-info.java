module com.bitsmi.ocp11.appendix.a.module2 
{
	// Export this package only to module 1
	exports com.bitsmi.opc11.appendix.a.module2.impl to com.bitsmi.ocp11.appendix.a.module1; 
	
	// Exposes slf4j module to dependent modules
	requires transitive slf4j.api;	
}