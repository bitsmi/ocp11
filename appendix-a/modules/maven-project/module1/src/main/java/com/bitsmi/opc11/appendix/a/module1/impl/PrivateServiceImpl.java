package com.bitsmi.opc11.appendix.a.module1.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bitsmi.opc11.appendix.a.module2.impl.Module2ServiceImpl;

public class PrivateServiceImpl 
{
	Logger log = LoggerFactory.getLogger(Module2ServiceImpl.class);
	
	public String sayHello()
	{
		String message = "Hello from module 1";
		log.info(message);
		
		return message;
	}
}
