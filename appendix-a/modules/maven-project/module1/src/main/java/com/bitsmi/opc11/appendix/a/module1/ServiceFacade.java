package com.bitsmi.opc11.appendix.a.module1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bitsmi.opc11.appendix.a.module1.impl.PrivateServiceImpl;
import com.bitsmi.opc11.appendix.a.module2.impl.Module2ServiceImpl;

public class ServiceFacade 
{
	Logger log = LoggerFactory.getLogger(ServiceFacade.class);
	
	public String sayHelloModule1()
	{
		return new PrivateServiceImpl().sayHello();
	}
	
	public String sayHelloModule2()
	{
		return new Module2ServiceImpl().sayHello();
	}
}
