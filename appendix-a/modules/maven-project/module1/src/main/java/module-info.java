module com.bitsmi.ocp11.appendix.a.module1 
{
	requires com.bitsmi.ocp11.appendix.a.module2;
	// Not needed as it's transitive from module2
	// requires slf4j.api;
	
	exports com.bitsmi.opc11.appendix.a.module1;
}