module com.bitsmi.ocp11.appendix.a.application 
{
	/* Only exported packages can be used inside this application. 
	 * This means that com.bitsmi.ocp11.appendix.a.module1 can be used but
	 * com.bitsmi.ocp11.appendix.a.module1.impl not because is not exported
	 * com.bitsmi.opc11.appendix.a.module2.impl cannot be used too because it's only exported to module1
	 */
	requires com.bitsmi.ocp11.appendix.a.module1;
}