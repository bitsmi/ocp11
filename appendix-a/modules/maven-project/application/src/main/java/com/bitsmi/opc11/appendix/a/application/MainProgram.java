package com.bitsmi.opc11.appendix.a.application;

//Cannot use logger API because slf4j module cannot be seen from module1. module2 require statement is not transitive.
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.bitsmi.opc11.appendix.a.module1.ServiceFacade;
// ERROR package com.bitsmi.opc11.appendix.a.module1.impl from module1 is not accessible
//import com.bitsmi.opc11.appendix.a.module1.impl.PrivateServiceImpl;
// ERROR package com.bitsmi.opc11.appendix.a.module2.impl is only accessible to module1
//import com.bitsmi.opc11.appendix.a.module2.impl.Module2ServiceImpl;

public class MainProgram 
{
	public static final void main(String... args)
	{
		// Logger API is not accessible
//		Logger log = LoggerFactory.getLogger(MainProgram.class);
//		log.info("Hello from MainProgram");
		
		ServiceFacade facade = new ServiceFacade();
		facade.sayHelloModule1();
		facade.sayHelloModule2();
		// PrivateServiceImpl is not accessible
//		new PrivateServiceImpl().sayHello();
		// Module2ServiceImpl is not accessible
//		new Module2ServiceImpl().sayHello();
	}
}
