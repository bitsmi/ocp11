# OCP11 - Appendix A: Modules

## Plain managed modules

The `plain-project` folder contains the source code for the example of how to manage JPMS modules through java command line.

#### Compile modules:

```sh
# Multiple modules must be comma separated without spaces between module names
javac -d build --module-source-path . --module com.bitsmi.ocp11.appendix.a.multi.main,com.bitsmi.ocp11.appendix.a.multi.service
```

#### Package as JAR file

Every module must be packaged into it's own JAR file. Output folder must exists.

```sh
jar -c --file=bin/main-module.jar -C build/com.bitsmi.ocp11.appendix.a.multi.main .
jar -c --file=bin/service-module.jar -C build/com.bitsmi.ocp11.appendix.a.multi.service .
# It's also possible to indicate the Main Class
jar -c --file=bin/main-module.jar --main-class com.bitsmi.ocp11.appendix.a.multi.main.MainProgram -C build/com.bitsmi.ocp11.appendix.a.multi.main .
```

Where:
* The -c argument tells jar to create a new JAR file.
* The --file argument tells the path of the output file - the created JAR file. Any directories you want the output JAR file to be under must already exist!
* The -C (uppercase C) argument tells the jar command to change directory to compiled module root directory and then include everything found in that directory - due to the following . argument (which signals "current directory").

#### Packing a Java Module as a Standalone Application

```sh
jlink --module-path "bin;%JAVA_HOME%\jmods" --add-modules com.bitsmi.ocp11.appendix.a.multi.main,com.bitsmi.ocp11.appendix.a.multi.service --output standalone-app
```

#### Run the program:

```sh
# Run from compilation folder
java --module-path build --module com.bitsmi.ocp11.appendix.a.multi.main/com.bitsmi.ocp11.appendix.a.multi.main.MainProgram
# Run from jar (without Main class specified)
java --module-path bin --module com.bitsmi.ocp11.appendix.a.multi.main/com.bitsmi.ocp11.appendix.a.multi.main.MainProgram
# Run from jar (with Main class specified)
java --module-path bin --module com.bitsmi.ocp11.appendix.a.multi.main
# Run standalone app
standalone-app\bin\java --module com.bitsmi.ocp11.appendix.a.multi.main/com.bitsmi.ocp11.appendix.a.multi.main.MainProgram
```  

## Maven managed modules

`maven-project` folder contains an example of 3 defined JPMS modules defined as an a Maven multi-module project. The module structure is the following:

- `module2` exposes a service implementation to `module1`. This implementation can be used only by `module1`. 
- `module2` exposes transitively the module `slf4j.api` to all modules that depends on him so they don't need to declare the require on it.
- `module1` requires `module2` in order to access it's service implementation and the `slf4j api`.
- `module1` exports `com.bitsmi.opc11.appendix.a.module1` package to everybody so the can use `ServiceFacade`. `com.bitsmi.opc11.appendix.a.module1.impl` remains private so external modules cannot use it.
- `application` requires `module1`. It can use `ServiceFacade` from module `module1` but not the `Logger` API from `slf4j` required in `module2` because `module1` require statement of `module2` is not transitive.

Execute the following commands to run the example:

#### Build and package

```sh
mvn clean package
``` 

#### Run the application

```
java --module-path "application\target\ocp11-appendix-a-application-1.0.0.jar;application\target\lib" --module com.bitsmi.ocp11.appendix.a.application
```


