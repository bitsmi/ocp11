package com.bitsmi.ocp11.appendix.a.multi.main;

import com.bitsmi.ocp11.appendix.a.multi.service.SampleService;

public class MainProgram
{
	public static final void main(String... args)
	{
		new SampleService().sayHello();
	}
}