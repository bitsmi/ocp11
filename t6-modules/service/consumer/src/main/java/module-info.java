import com.bitsmi.ocp11.t6.service.spi.ILanguageTranslationService;

module com.bitsmi.ocp11.t6.service.consumer 
{
	// Require both service provider interface and service provider modules
	requires com.bitsmi.ocp11.t6.service.spi;
	requires com.bitsmi.ocp11.t6.service.impl;
	
	uses ILanguageTranslationService;
}