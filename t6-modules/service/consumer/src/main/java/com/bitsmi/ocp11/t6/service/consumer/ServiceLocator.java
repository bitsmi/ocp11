package com.bitsmi.ocp11.t6.service.consumer;

import java.util.Map;
import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.bitsmi.ocp11.t6.service.spi.ILanguageTranslationService;

public class ServiceLocator 
{
	private static ServiceLocator instance;
	
	private Map<String, ILanguageTranslationService> cache;
	
	public static ServiceLocator getInstance()
	{
		if(instance==null) {
			instance = new ServiceLocator();
			instance.loadCache();
		}
		return instance;
	}
	
	public ILanguageTranslationService getLanguageTranslationService(String srcLanguage)
	{
		return cache.get(srcLanguage);
	}
	
	private void loadCache()
	{
		this.cache = ServiceLoader.load(ILanguageTranslationService.class)
				.stream()
				.map(Provider::get)
				.collect(Collectors.toMap(ILanguageTranslationService::getLanguage, Function.identity()));
	}
}
