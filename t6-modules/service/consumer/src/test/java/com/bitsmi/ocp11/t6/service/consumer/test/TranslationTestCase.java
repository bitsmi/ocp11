package com.bitsmi.ocp11.t6.service.consumer.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.bitsmi.ocp11.t6.service.consumer.ServiceLocator;
import com.bitsmi.ocp11.t6.service.spi.ILanguageTranslationService;
import com.bitsmi.ocp11.t6.service.spi.dto.TranslationDto;

public class TranslationTestCase 
{
	@Test
	public void englishTranslationTest()
	{
		ILanguageTranslationService translationService = ServiceLocator.getInstance().getLanguageTranslationService("English");
		TranslationDto result = translationService.translate("Spanish", "Casa");
		
		System.out.println("RESULT: " + result.getTranslatedWord());
		
		assertThat(translationService.getLanguage()).isEqualTo("English");
		assertThat(result.getSourceLanguage()).isNotNull();
		assertThat(result.getSourceWord()).isNotNull();
		assertThat(result.getTranslationLanguage()).isEqualTo("English");
		assertThat(result.getTranslatedWord()).isEqualTo("House");
	}
	
	@Test
	public void germanTranslationTest()
	{
		ILanguageTranslationService translationService = ServiceLocator.getInstance().getLanguageTranslationService("German");
		TranslationDto result = translationService.translate("Spanish", "Casa");
		
		System.out.println("RESULT: " + result.getTranslatedWord());
		
		assertThat(translationService.getLanguage()).isEqualTo("German");
		assertThat(result.getSourceLanguage()).isNotNull();
		assertThat(result.getSourceWord()).isNotNull();
		assertThat(result.getTranslationLanguage()).isEqualTo("German");
		assertThat(result.getTranslatedWord()).isEqualTo("Haus");
	}
}
