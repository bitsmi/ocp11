import com.bitsmi.ocp11.t6.service.impl.EnglishTranstationServiceImpl;
import com.bitsmi.ocp11.t6.service.impl.GermanTranslationServiceImpl;
import com.bitsmi.ocp11.t6.service.spi.ILanguageTranslationService;

module com.bitsmi.ocp11.t6.service.impl 
{
	requires com.bitsmi.ocp11.t6.service.spi;
	
	// Export of "impl" package is not required because services implementations are provided
	
	provides ILanguageTranslationService with EnglishTranstationServiceImpl, GermanTranslationServiceImpl;
}