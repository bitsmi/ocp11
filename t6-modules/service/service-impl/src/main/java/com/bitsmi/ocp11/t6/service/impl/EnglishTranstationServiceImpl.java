package com.bitsmi.ocp11.t6.service.impl;

import com.bitsmi.ocp11.t6.service.spi.ILanguageTranslationService;
import com.bitsmi.ocp11.t6.service.spi.dto.TranslationDto;

public class EnglishTranstationServiceImpl implements ILanguageTranslationService 
{
	@Override
	public String getLanguage()
	{
		return "English";
	}
	
	@Override
	public TranslationDto translate(String srcLanguage, String word)
	{
		String translatedWord = null;
		switch(srcLanguage) {
			case "Spanish":
				translatedWord = translateFromSpanish(word);
				break;
			case "German":
				translatedWord = translateFromGerman(word);
				break;
			default:
				throw new IllegalArgumentException("Unknown language " + srcLanguage);
		}
		
		return new TranslationDto()
				.setSourceLanguage(srcLanguage)
				.setSourceWord(word)
				.setTranslationLanguage(getLanguage())
				.setTranslatedWord(translatedWord);
	}
	
	private String translateFromSpanish(String word)
	{
		String translatedWord = null;
		switch(word.toLowerCase()) {
			case "casa":
				translatedWord = "House";
				break;
			case "coche":
				translatedWord = "Car";
				break;
			case "persona":
				translatedWord = "Person";
				break;
			default:
				break;
		}
		
		return translatedWord;
	}
	
	private String translateFromGerman(String word)
	{
		String translatedWord = null;
		switch(word.toLowerCase()) {
			case "casa":
				translatedWord = "Haus";
				break;
			case "coche":
				translatedWord = "Auto";
				break;
			case "persona":
				translatedWord = "Mensch";
				break;
			default:
				break;
		}
		
		return translatedWord;
	}
}	
