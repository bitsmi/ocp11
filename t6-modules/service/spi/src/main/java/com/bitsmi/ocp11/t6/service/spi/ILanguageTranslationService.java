package com.bitsmi.ocp11.t6.service.spi;

import com.bitsmi.ocp11.t6.service.spi.dto.TranslationDto;

public interface ILanguageTranslationService 
{
	public String getLanguage();
	
	public TranslationDto translate(String srcLanguage, String word);
}
