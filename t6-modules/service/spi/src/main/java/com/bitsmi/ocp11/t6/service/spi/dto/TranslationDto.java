package com.bitsmi.ocp11.t6.service.spi.dto;

public class TranslationDto 
{
	private String sourceLanguage;
	private String sourceWord;
	private String translationLanguage;
	private String translatedWord;

	public String getSourceLanguage() 
	{
		return sourceLanguage;
	}

	public TranslationDto setSourceLanguage(String sourceLanguage) 
	{
		this.sourceLanguage = sourceLanguage;
		return this;
	}

	public String getSourceWord() 
	{
		return sourceWord;
	}
	
	public TranslationDto setSourceWord(String sourceWord) 
	{
		this.sourceWord = sourceWord;
		return this;
	}
	
	public String getTranslationLanguage() 
	{
		return translationLanguage;
	}
	
	public TranslationDto setTranslationLanguage(String translationLanguage) 
	{
		this.translationLanguage = translationLanguage;
		return this;
	}
	
	public String getTranslatedWord() {
		return translatedWord;
	}

	public TranslationDto setTranslatedWord(String translatedWord) {
		this.translatedWord = translatedWord;
		return this;
	}
	
	public boolean isTranslationFound()
	{
		return translatedWord!=null;
	}
}
